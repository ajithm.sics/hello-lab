import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        // opacity: 0.5
    },
    loginForm: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },
    logoImage: {
        width: 50,
        height: 170
    },
    loginView: {
        height: "100%",
        width: '100%',
        // alignItems: 'center',
        // justifyContent: 'center',
        // paddingVertical: 35,
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.8,
        // shadowRadius: 6,
        // borderColor: '#c8ccd0',
        // borderTopColor: '#c8ccd0',
        // borderWidth: 1,
        // elevation: 4,
        // borderRadius: 7,
    },
    loginText: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
        // maxWidth: 10,
        maxHeight: 40,
        paddingLeft: 10,
        // paddingVertical: 5,
        // borderColor: '#8ad9f8',
        backgroundColor: '#dee4ea',
        width: '100%',
        // borderWidth: 1,

        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.4,
        // shadowRadius: 6,
        // borderColor: '#c8ccd0',
        // borderTopColor: '#c8ccd0',
        // borderWidth: 0.5,
        // elevation: 4,
        // borderRadius: 7,
    },
    passwordSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        // maxWidth: 10,
        maxHeight: 45,
        paddingLeft: 10,
        borderRadius: 8,
        backgroundColor: '#dee4ea',
    },
    input: {
        flex: 1,
        // fontWeight: '900',
        color: '#202020',
        paddingLeft: 10,
        fontSize: 13,
        // paddingVertical: 5,
    },
    registerButton: {
        marginTop: 25,
        backgroundColor: '#1fa274',
        paddingVertical: 10,
        alignItems: 'center',
        borderRadius: 15
        // position: 'absolute',
        // right: 0
        // alignItems: 'center',
    },
    driverButton: {
        marginTop: 25,
        backgroundColor: '#202020',
        paddingVertical: 8,
        alignItems: 'center',
        paddingHorizontal: 25,
        // borderRadius: 15,
        width: "45%",
        marginLeft: 10,
    },
    customerButton: {
        marginTop: 25,
        backgroundColor: '#202020',
        paddingVertical: 8,
        alignItems: 'center',
        // borderRadius: 15,
        paddingHorizontal: 25,
        width: "45%"
    },
    updateButton: {
        marginTop: 25,
        backgroundColor: '#202020',
        paddingVertical: 8,
        alignItems: 'center',
        paddingHorizontal: 25,
        // borderRadius: 15,
        width: "100%"
    },
    resetButton: {
        marginTop: 25,
        backgroundColor: '#202020',
        paddingVertical: 8,
        alignItems: 'center',
        // borderRadius: 15,
        paddingHorizontal: 25,
        marginLeft: 10,
        width: "45%"
    },
    mainContainer: {
        backgroundColor: '#202020',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 15
    },
})
