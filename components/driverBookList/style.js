import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
  mainContainer: {
    backgroundColor: '#202020',
    height: 75,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subContainer: {
    backgroundColor: '#ffffff',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    // justifyContent: 'center',
    marginTop: -25,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    paddingHorizontal: 10
  },
  cardView: {
    width: '100%',
    shadowColor: '#000',
    shadowOffset: { width: 7, height: 7 },
    shadowOpacity: 0.4,
    shadowRadius: 10,
    borderRadius: 15,
    borderColor: '#c8ccd0',

    borderTopColor: '#c8ccd0',
    // paddingHorizontal: 10,
    // paddingVertical: 20,
    // borderWidth: 1,
    elevation: 8,
    marginBottom: 15,
    marginTop: 15,
    backgroundColor: '#eef3fe',
  },
  secondCardView: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    // shadowColor: '#000',
    // shadowOffset: { width: 7, height: 7 },
    // shadowOpacity: 0.4,
    // shadowRadius: 10,
    borderRadius: 15,
    // borderColor: '#c8ccd0',
    // borderTopColor: '#c8ccd0',
    paddingHorizontal: 10,
    borderTopLeftRadius: 50,
    borderBottomRightRadius: 50,
    paddingVertical: 20,
    // borderWidth: 1,
    // elevation: 8,
    // marginBottom: 10,
    // marginTop: 10,
    backgroundColor: '#ffffff',
    // marginHorizontal: 10
  },
  subContainer: {
    backgroundColor: '#f7f7f7',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    // justifyContent: 'center',
    marginTop: -25,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
  },
  loginView: {
    marginTop: 10
    // height: "100%",
    // width: '100%',
  }
})