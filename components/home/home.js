import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View, BackHandler,
    Text,
    StatusBar,
    Image, ActivityIndicator,
    TouchableOpacity,
    TextInput,
    ImageBackground, AsyncStorage,
    ToastAndroid,
} from 'react-native';
import styles from './homeStyle';
import DatePicker from 'react-native-datepicker';
import Moment from 'moment';
// import mainStyles from '../mainStyle';
// import { createStackNavigator } from '@react-navigation/stack';
// import { NavigationContainer } from '@react-navigation/native';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Email: '',
            Password: '',
            date:"2016-05-15",
            logout: 'no',
        }
    }

    driverBooking = () => {
        this.props.navigation.navigate('DriverBooking')
    }

    taxiBooking = () => {
        this.props.navigation.navigate("TaxiBooking")
    }

    componentDidMount() {
        AsyncStorage.setItem("logout", JSON.stringify(this.state.logout));
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        // this.props.navigation.goBack(null);
        BackHandler.exitApp();
        return true;
    }

    render() {
        const today = new Date();
        return (
            <View>
                <View style={styles.mainContainer}>
                    <Text style={{ marginTop: -20, letterSpacing: 1, fontSize: 14, color: "#ffffff" }}>BOOKINGS</Text>
                </View>
                <View style={styles.subContainer}>

                    <TouchableOpacity onPress={this.driverBooking} style={styles.driverBooking}>
                        {/* <View style={styles.blueLine}>
                        </View> */}
                        <View style={styles.rightBlueView}>
                            <Text style={styles.driverBookText}>Driver Booking</Text>
                            {/* <View style={{ display: 'flex', width: '100%', flexDirection: 'row', overflow: 'hidden' }}>
                                <View style={{ width: '25%' }}>
                                    <Image
                                        source={require('../../assets/images/driver.png')}
                                        style={{ width: 60, height: 80 }} />
                                </View>
                                <View style={{ width: '75%' }}>
                                    <Text style={styles.loremText}>Kerala Wings is the premier organisation that can take care of your cab requirements.</Text>
                                </View>
                            </View> */}
                        </View>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={this.taxiBooking} style={styles.taxiBooking}>
                        {/* <View style={styles.orangeLine}>
                        </View> */}
                        <View style={styles.rightOrangeView}>
                            <Text style={styles.taxiBookText}>Taxi Booking</Text>
                            {/* <View style={{ display: 'flex', width: '100%', flexDirection: 'row', overflow: 'hidden' }}>
                                <View style={{ width: '70%' }}>
                                <Text style={styles.loremText}>we have prospered to become among the very best call cab and call taxi service in Trivandrum.</Text>
                                </View>
                                <View style={{ width: '30%', backgroundColor: '#ffffff' }}>
                                    <Image
                                        source={require('../../assets/images/taxi.png')}
                                        style={{ width: 90, height: 90 }} />
                                </View>
                            </View> */}
                        </View>
                    </TouchableOpacity>

                </View>

            </View>
        )
    }
}
export default Home;