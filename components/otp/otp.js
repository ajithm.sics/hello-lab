import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar, Alert,
    Image,
    TouchableOpacity,
    TextInput,
    ImageBackground,
    ToastAndroid
} from 'react-native';

import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import styles from './otpStyle';
import OTPInputView from '@twotalltotems/react-native-otp-input'
class Otp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code: ''
        }
    }
    getOtp(otp) {

        // this.props.navigation.navigate('Register', {otp: responseJson.otp})
    }
    homego = () => {
        console.log('fffffffffff')
    }

    componentDidMount() {

    }

    render() {
        let gotopage = this.state.code
        console.log(this.props.route.params, 'hello res otp page')
        return (
            <View>
                <View>
                    <View style={styles.imageContainer} style={{ width: '100%', height: '100%' }}>
                        <ImageBackground source={require('../../assets/images/otpCar.jpg')} style={{ width: '100%', height: '80%' }}>

                        </ImageBackground>
                        <View style={styles.RegisterpageBody}>
                            <Text style={styles.MainHeading}>OTP</Text>
                            <Text style={styles.SubHeading}>Enter your otp number to proceed</Text>

                            <View style={{ marginTop: 30 }}>
                                <Text style={styles.otpheading}>6 Digit OTP Number</Text>
                                <Text>{this.props.route.params.otp}</Text>
                                <OTPInputView
                                    style={{ width: '100%', height: 70 }}
                                    pinCount={6}
                                    code={this.state.code}
                                    onCodeChanged={code => { this.setState({ code }) }}
                                    getOtp={(otp) => this.getOtp(otp)}
                                    autoFocusOnLoad
                                    codeInputFieldStyle={styles.underlineStyleBase}
                                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                    onCodeFilled={(code => {
                                        console.log(`Code is ${code}, you are good to go!`)
                                        if (this.props.route.params.otp == code && (this.props.route.params.name == '' || this.props.route.params.name == null)) {
                                            this.props.navigation.navigate('Register')
                                            ToastAndroid.showWithGravity(
                                                'Login successfully',
                                                ToastAndroid.SHORT,
                                                ToastAndroid.CENTER,
                                            )
                                        }
                                        else if (this.props.route.params.otp == code && this.props.route.params.name !== '') {
                                            this.props.navigation.navigate('Bhome')
                                        }
                                        else {
                                            ToastAndroid.showWithGravity(
                                                'Please enter correct OTP',
                                                ToastAndroid.SHORT,
                                                ToastAndroid.CENTER,
                                            )
                                        }
                                    })}
                                />
                            </View>

                        </View>
                    </View>
                    {/* } */}
                </View>
                {/* } */}
            </View>
        )
    }
}
export default Otp;