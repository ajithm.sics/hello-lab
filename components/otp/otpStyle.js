import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  imageContainer: {
    flex: 1,
    flexDirection: "column"
},
    bgimage: {
        width: '100%',
        height: 700,
    },
    RegisterpageBody: {
        width: '100%',
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#ffffff',
        padding: 25,
    },
    MainHeading: {
        fontSize: 20,
        fontWeight: "bold",
        color: '#000000',
        letterSpacing: 1
    },
    SubHeading: {
        marginTop: 10,
        opacity: 0.5
    },
    otpheading: {
        marginTop: 10,
        color: '#000000',
        fontSize: 18
    },
    input: {
        fontSize: 20,
        paddingBottom: 5,
        borderRadius: 3,
        borderColor: "#353131",
        borderBottomWidth: 1,
        minWidth: 80,
        color: '#353131',
        fontWeight: "bold",
        letterSpacing: 2,
      },
      borderStyleBase: {
        width: 30,
        height: 45
      },
    
      borderStyleHighLighted: {
        borderColor: "#000000",
        color: '#000000',
      },
    
      underlineStyleBase: {
        width: 40,
        height: 50,
        borderWidth: 0,
        borderBottomWidth: 2,
        borderColor: "#000000",
        color: '#000000',
        fontSize: 22
      },
    
      underlineStyleHighLighted: {
        borderColor: "#000000",
        color: '#000000',
      },
})