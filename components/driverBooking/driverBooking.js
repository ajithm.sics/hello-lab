import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View, Picker,
    Text,
    StatusBar, Alert,
    Image, ActivityIndicator,
    TouchableOpacity, BackHandler,
    TextInput, TimePicker,
    ImageBackground, AsyncStorage,
    ToastAndroid,
} from 'react-native';
import styles from './driverBookingStyle';
import DatePicker from 'react-native-datepicker';
import Moment from 'moment';
// import mainStyles from '../mainStyle';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';

class DriverBooking extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            firstName: '',
            phone: '',
            landnumber: '',
            vehicleName: '',
            vehicleType: '',
            vehicleNumber: '',
            address: '',
            startDateTime: new Date(),
            endDateTime: new Date(),
            location: '',
            destination: '',
            driver: '',
            bookingDate: Moment().format("DD-MM-YYYY"),
            numberofDays: '',
            endDate: '',
            remarks: '',
            vehicle: 'Automatic',
            status: '',
            clickTime: false,
            StartTime: new Date(),
            mode: 'time',
            StartTimeShow: false,
            driversList: [],
        }
    }


    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        this.props.navigation.navigate('Home')
        return true;
    }

    componentDidMount() {
        var ref = this;
        AsyncStorage.getItem('userdetails', async (err, result) => {
            var res = await JSON.parse(result);
            console.log(res, 'responseJson userdetails service engin')
            this.setState({
                userId: res.id,
                firstName: res.name,
                location: res.location,
                phone: res.phone,
                landnumber: res.landPhone,
                address: res.address,
                vehicleName: res.vehicleName,
                // vehicleType: res.vehicleType,
                vehicleNumber: res.vehicleNumber,
            })
        });
    }

    numberChange = (daysNumber) => {

        console.log(daysNumber, 'ajith daysNumber')
        var getToday = new Date()
        var todayDate = getToday.getDate(); //To get the Current Date
        var todayMonth = getToday.getMonth() + 1; //To get the Current Month
        var todayYear = getToday.getFullYear(); //To get the Current Year
        var countToday = todayDate + '-' + todayMonth + '-' + todayYear
        if (daysNumber < 1 || daysNumber == null) {
            console.log(daysNumber, 'ajith inside')
            this.setState({
                endDate: countToday,
                numberofDays: daysNumber,
            })
        }
        else {
            // jan 01 1970
            var numberCalculate = daysNumber - 1
            var number = parseInt(numberCalculate)
            var selectD = this.state.bookingDate.split('-');
            var selecteDate = selectD[2] + '-' + selectD[1] + '-' + selectD[0]
            var date1 = new Date(selecteDate);
            date1.setDate(date1.getDate() + number)

            var cDate = date1.getDate(); //To get the Current Date
            var cMonth = date1.getMonth() + 1; //To get the Current Month
            var cYear = date1.getFullYear(); //To get the Current Year
            var chooseEndDate = cDate + '-' + cMonth + '-' + cYear

            this.setState({
                endDate: chooseEndDate,
                numberofDays: daysNumber,
            })

        }
        this.setState({
            numberofDays: daysNumber,
        })
        console.log(this.state.bookingDate, 'ajith ')
        console.log(chooseEndDate, 'ajith endDate')

        fetch('https://keralawings.co.in/booking/api/driver_list', {
            method: 'post',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: 'start_date=' + this.state.bookingDate + '&end_date=' + chooseEndDate


        }).then((response) => response.json())
            .then(async (responseJson) => {
                console.log(responseJson, 'ajith responseJson')
                if (responseJson.status == true) {
                    this.setState({
                        spinner: false,
                        driversList: responseJson.driverlist
                    })
                }
                else {
                    this.setState({
                        spinner: false,
                    })
                }
            }).catch((error) => {
                console.warn(error, 'error');
            });

    }

    bookingdate = () => {

    }

    SelectStartingTime = () => {
        console.log(this.state.startDateTime, 'start click')
        this.setState({
            // mode: 'time',
            StartTimeShow: true
        })
    }

    // onChangeStartDate = (event, selectedDate) => {
    //     console.log(this.state.startDateTime, 'start click change')
    //     const currentDate = selectedDate;
    //     this.setState({
    //         startDateTime: currentDate,
    //         StartTimeShow: false
    //     })
    // };

    clickBooking = () => {
        this.setState({ error: true, spinner: true })
        console.log(this.state.firstName, 'start click firstName')
        console.log(this.state.phone, 'start click phone')
        console.log(this.state.vehicleName, 'start click vehicleName')
        console.log(this.state.landnumber, 'start click landnumber')
        console.log(this.state.vehicleType, 'start click vehicleType')

        console.log(this.state.vehicleNumber, 'start click vehicleNumber')
        console.log(this.state.address, 'start click address')
        console.log(this.state.location, 'start click location')
        console.log(this.state.destination, 'start click destination')

        console.log(this.state.driver, 'start click driver')
        console.log(this.state.bookingDate, 'start click bookingDate')
        console.log(this.state.numberofDays, 'start click numberofDays')
        console.log(this.state.endDate, 'start click endDate')
        console.log(this.state.remarks, 'start click remarks')
        console.log(this.state.vehicle, 'start click vehicle')
        console.log(this.state.StartTime, 'start click StartTimes')


        var a = this.state.bookingDate.split('-');
        var bookingDate = a[2] + '-' + a[1] + '-' + a[0]

        var b = this.state.endDate.split('-');
        var endDate = b[2] + '-' + b[1] + '-' + b[0]
        console.log(bookingDate, 'start click bookingDate')
        console.log(endDate, 'start click bookingDate')
        if (this.state.firstName == "" || this.state.phone == "" || this.state.vehicleName == "" ||
            this.state.landnumber == "" || this.state.landnumber == null || this.state.vehicleType == "" ||
            this.state.vehicleNumber == "" || this.state.address == "" || this.state.location == "" ||
            this.state.destination == "" || this.state.driver == "" || this.state.bookingDate == "" ||
            this.state.numberofDays == "" || this.state.endDate == "" || this.state.remarks == "" ||
            this.state.vehicle == "" || this.state.StartTime == "") {
            ToastAndroid.showWithGravity(
                'Please fill the mandatory Fields',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
            this.setState({ spinner: false })
        }
        else {
            fetch('https://keralawings.co.in/booking/api/api_driverbooking_save', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'customer_name=' + this.state.firstName + '&customer_number=' + this.state.phone + '&landnumber=' + this.state.landnumber
                    + '&address=' + this.state.address + '&vehicle_name=' + this.state.vehicleName + '&vehicle_type=' + this.state.vehicleType
                    + '&vehicle_number=' + this.state.vehicleNumber + '&location=' + this.state.location + '&destination=' + this.state.destination
                    + '&driver=' + this.state.driver + '&vehicle=' + this.state.vehicle + '&remarks=' + this.state.remarks
                    + '&booking_date=' + bookingDate + '&timepicker=' + this.state.StartTime + '&end_date=' + endDate
                    + '&no_day=' + this.state.numberofDays + '&status=' + this.state.status

            }).then((response) => response.json())
                .then(async (responseJson) => {
                    console.log(responseJson, 'start click booking response');
                    if (responseJson.status == true) {
                        ToastAndroid.showWithGravity(
                            'Driver booked Successfully',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                        this.setState({
                            spinner: false
                        })
                    }
                    else {
                        ToastAndroid.showWithGravity(
                            'Booking Error',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                        alert(responseJson.msg)
                        this.setState({
                            spinner: false,
                        })
                    }


                }).catch((error) => {
                    console.warn(error, 'error');
                });
        }
    }

    selectTime = () => {
        this.setState({
            StartTimeShow: true,
            StartTime: new Date(),
            clickTime: false
        })
        console.log(this.state.StartTime, 'StartTime')
    }

    onChange = (event, selectedDate) => {
        // var hours = new Date().getHours();
        var chooseDate = selectedDate;
        var hours = chooseDate.getHours();
        var minutes = chooseDate.getMinutes();
        var h = hours.toString();
        var colon = ":";
        var hc = h.concat(colon)
        var m = minutes.toString();
        var time = hc.concat(m)
        this.setState({
            StartTime: time,
            StartTimeShow: false,
            clickTime: true,
        })
    };

    render() {
        const today = new Date();
        return (
            <View style={{ backgroundColor: '#f5f5f5' }}>
                {this.state.spinner ?
                    <View style={{ marginTop: "80%", position: 'absolute', zIndex: 99, width: "100%", paddingHorizontal: 20, height: 70 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', }}>
                            <Image
                                source={require('../../assets/images/loader.gif')}
                                style={{ width: 200, height: 200 }} />
                            {/* <Text style={mainStyles.spinnerTextStyle}>please wait</Text> */}
                        </View>
                    </View>
                    :
                    <View>
                        <View style={styles.mainContainer}>
                            <Text style={{ marginTop: -20, letterSpacing: 1, fontSize: 14, color: "#ffffff" }}>DRIVER BOOKING</Text>
                        </View>
                        <View style={styles.subContainer}>
                            <View style={styles.loginView}>
                                <ScrollView contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 20, position: 'relative', zIndex: -1, paddingBottom: 225 }}>
                                    {/* <View style={{ alignItems: 'center', marginTop: 30 }}>
                                        <Image
                                            source={require('../../assets/images/icon2.png')}
                                            style={{ width: 100, height: 100 }} />
                                    </View> */}

                                    {/* <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="First Name"
                                            placeholderTextColor="#919e9a"
                                            value={this.state.firstName}
                                            onChangeText={(value) => { this.setState({ firstName: value }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.firstName == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the first Name</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    } */}

                                    {/* <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Phone number"
                                            keyboardType='numeric'
                                            placeholderTextColor="#919e9a"
                                            value={this.state.phone}
                                            onChangeText={(value) => { this.setState({ phone: value }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.phone == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the phone number</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    } */}

                                    {/* <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Land number"
                                            keyboardType='numeric'
                                            placeholderTextColor="#919e9a"
                                            value={this.state.landnumber}
                                            onChangeText={(value) => { this.setState({ landnumber: value }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.landnumber == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Land number</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    } */}

                                    {/* <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Vehicle Name"
                                            placeholderTextColor="#919e9a"
                                            value={this.state.vehicleName}
                                            onChangeText={(value) => { this.setState({ vehicleName: value }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.vehicleName == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the vehicle name</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    } */}

                                    {/* <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Vehicle Type"
                                            placeholderTextColor="#919e9a"
                                            value={this.state.vehicleType}
                                            onChangeText={(value) => { this.setState({ vehicleType: value }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.vehicleType == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the vehicle type</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    } */}

                                    {/* <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Vehicle Number"
                                            placeholderTextColor="#919e9a"
                                            value={this.state.vehicleNumber}
                                            onChangeText={(value) => { this.setState({ vehicleNumber: value }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.vehicleNumber == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the vehicle number</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    } */}

                                    {/* <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Address"
                                            // keyboardType='numeric'
                                            placeholderTextColor="#919e9a"
                                            value={this.state.address}
                                            onChangeText={(value) => { this.setState({ address: value }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.address == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Address</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    } */}
                                    <DatePicker
                                        style={styles.Datepicker}
                                        date={this.state.bookingDate}
                                        // mode="date"
                                        mode={'date'}
                                        placeholder="Select Booking date"
                                        placeholderTextColor="#919e9a"
                                        format="DD-MM-YYYY"
                                        // format= {Moment}
                                        minDate={today}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateInput: { borderWidth: 0, borderColor: "#E09D00", borderBottomWidth: 2, alignItems: "flex-start" },
                                            dateTouchBody: { borderColor: "red", borderWidth: 0 },
                                            placeholderText: { fontSize: 13, color: "#757575" },
                                            dateText: { fontSize: 13, color: "black", textAlign: "left" },
                                            dateIcon: { position: 'absolute', right: 0, top: 12, bottom: 0, marginLeft: 0, height: 20, width: 20 },
                                        }}
                                        // onDateChange={(date) => { this.setState({ bookingDate: date }) }}
                                        onDateChange={(date) => { this.setState({ bookingDate: date }) }}
                                    />
                                    {this.state.error && <View>
                                        {this.state.bookingDate == '' ? <Text style={{ color: 'red', paddingLeft: 5 }}>Please enter the Booking Date</Text> : <Text style={{ display: 'none' }}></Text>}
                                    </View>}

                                    {this.state.clickTime == false &&
                                        <View style={styles.inputView}>
                                            <TouchableOpacity style={styles.timeButton} onPress={this.selectTime}>
                                                <Text style={{ fontSize: 12 }}>Select Time</Text>
                                            </TouchableOpacity>
                                        </View>
                                    }

                                    {this.state.clickTime == true &&
                                        <View style={styles.inputView}>
                                            <TouchableOpacity style={styles.timeButton} onPress={this.selectTime}>
                                                <Text>{this.state.StartTime}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    }
                                    {this.state.error &&
                                        <View>
                                            {this.state.clickTime == false ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please select time</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    {this.state.StartTimeShow == true &&
                                        <DateTimePicker
                                            testID="dateTimePicker"
                                            value={this.state.StartTime}
                                            mode={this.state.mode}
                                            // is24Hour={true}
                                            display="default"
                                            onChange={this.onChange}
                                        />
                                    }


                                    <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Number of days"
                                            keyboardType='numeric'
                                            placeholderTextColor="#919e9a"
                                            value={this.state.numberofDays}
                                            // onChangeText={(daysNumber) => { this.setState({ numberofDays: daysNumber }) }}
                                            onChangeText={(daysNumber) => { this.numberChange(daysNumber) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.numberofDays == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Number of Days</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    <DatePicker
                                        style={styles.Datepicker}
                                        date={this.state.endDate}
                                        mode="date"
                                        placeholder="Select Ending date"
                                        placeholderTextColor="#919e9a"
                                        format="DD-MM-YYYY"
                                        minDate={today}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateInput: { borderWidth: 0, borderColor: "#E09D00", borderBottomWidth: 2, alignItems: "flex-start" },
                                            dateTouchBody: { borderColor: "red", borderWidth: 0 },
                                            placeholderText: { fontSize: 13, color: "#757575" },
                                            dateText: { fontSize: 13, color: "black", textAlign: "left" },
                                            dateIcon: { position: 'absolute', right: 0, top: 12, bottom: 0, marginLeft: 0, height: 20, width: 20 },
                                        }}
                                    // onDateChange={(date) => { this.setState({ endDate: date }) }}
                                    />
                                    {this.state.error && <View>
                                        {this.state.endDate == '' ? <Text style={{ color: 'red', fontSize: 10, paddingLeft: 5 }}>Please enter the Ending Date</Text> : <Text style={{ display: 'none' }}></Text>}
                                    </View>}

                                    <View style={styles.inputView}>
                                        <Picker
                                            style={styles.input}
                                            mode="dropdown"
                                            selectedValue={this.state.driver}
                                            onValueChange={(value) => { this.setState({ driver: value }) }}>
                                            {this.state.driversList.map((item) => {
                                                return (<Picker.Item label={item.first_name} value={item.id} key={item.id} />) //if you have a bunch of keys value pair
                                            })}
                                        </Picker>

                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.driver == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Driver</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Location"
                                            placeholderTextColor="#919e9a"
                                            value={this.state.location}
                                            onChangeText={(value) => { this.setState({ location: value }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.location == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Location</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    {/* <View style={styles.inputView}>
                                    <TextInput
                                        style={styles.input}
                                        placeholder="boarding point"
                                        placeholderTextColor="#1fa274"
                                        value={this.state.boardingPoint}
                                        onChangeText={(value) => { this.setState({ boardingPoint: value }) }}
                                    />
                                </View>
                                {this.state.error &&
                                    <View>
                                        {this.state.boardingPoint == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the boarding Point</Text> :
                                            <Text style={{ display: 'none' }}></Text>
                                        }
                                    </View>
                                } */}

                                    <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Destination"
                                            placeholderTextColor="#919e9a"
                                            value={this.state.destination}
                                            onChangeText={(value) => { this.setState({ destination: value }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.destination == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Destination</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    <View style={styles.inputView}>
                                        <Picker selectedValue={this.state.vehicle} style={styles.input} onValueChange={(value) => { this.setState({ vehicle: value }) }}>
                                            <Picker.Item label="Automatic" value="Automatic" />
                                            <Picker.Item label="Mannual" value="Mannual" />
                                        </Picker>
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.vehicle == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Vehicle</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    <View style={styles.inputView}>
                                        <Picker selectedValue={this.state.vehicleType} style={styles.input} onValueChange={(value) => { this.setState({ vehicleType: value }) }}>
                                            <Picker.Item label="Normal" value="NORMAL" />
                                            <Picker.Item label="Sedan" value="SEDAN" />
                                            <Picker.Item label="muv/suv" value="MUV/SUV" />
                                            <Picker.Item label="Premium" value="PREMIUM" />
                                            <Picker.Item label="Traveller" value="TRAVELLER" />
                                            <Picker.Item label="Bus" value="BUS" />
                                            <Picker.Item label="Commertial" value="COMMERTIAL" />
                                        </Picker>
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.vehicleType == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Vehicle</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    <View style={styles.inputView}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder="Remarks"
                                            placeholderTextColor="#919e9a"
                                            value={this.state.remarks}
                                            onChangeText={(value) => { this.setState({ remarks: value }) }}
                                        />
                                    </View>
                                    {this.state.error &&
                                        <View>
                                            {this.state.remarks == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Please enter the Remarks</Text> :
                                                <Text style={{ display: 'none' }}></Text>
                                            }
                                        </View>
                                    }

                                    <TouchableOpacity onPress={this.clickBooking} style={styles.registerButton}>
                                        <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: 'bold', letterSpacing: 0.5 }}>SUBMIT</Text>
                                    </TouchableOpacity>

                                </ScrollView>
                            </View>

                        </View>
                    </View>

                }
            </View>
        )
    }
}
export default DriverBooking;