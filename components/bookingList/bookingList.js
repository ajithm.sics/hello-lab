import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View, BackHandler,
    Text,
    StatusBar,
    Image, ActivityIndicator,
    TouchableOpacity,
    TextInput,
    ImageBackground, AsyncStorage,
    ToastAndroid,
} from 'react-native';
import styles from './bookingListStyle';

class BookingList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Email: '',
            Password: '',
            spinner: true,
        }
    }

    driverBookingList = () => {
        this.props.navigation.navigate("DriverBookList")
    }

    taxiBookingList = () => {
        this.props.navigation.navigate("TaxiBookList")
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    render() {
        return (
            <View>
                <View style={styles.mainContainer}>
                    <Text style={{ marginTop: -20, letterSpacing: 1, fontSize: 14, color: "#ffffff" }}>BOOKING LIST</Text>
                </View>
                <View style={styles.subContainer}>
                    <TouchableOpacity onPress={this.driverBookingList} style={styles.driverBooking}>
                        {/* <View style={styles.blueLine}>
                        </View> */}
                        <View style={styles.rightBlueView}>
                            <Text style={styles.driverBookText}>Driver Booking List</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.taxiBookingList} style={styles.taxiBooking}>
                        <View style={styles.rightOrangeView}>
                            <Text style={styles.taxiBookText}>Taxi Booking List</Text>
                            {/* <View style={{ display: 'flex', width: '100%', flexDirection: 'row', overflow: 'hidden' }}>
                                <View style={{ width: '70%' }}>
                                    <Text style={styles.loremText}>Our diligent group of drivers are experts in the various localities in and around Trivandrum.</Text>
                                </View>
                                <View style={{ width: '30%', backgroundColor: '#ffffff' }}>
                                    <Image
                                        source={require('../../assets/images/taxilist.jpg')}
                                        style={{ width: 90, height: 90 }} />
                                </View>
                            </View> */}
                        </View>
                    </TouchableOpacity>

                </View>

            </View>
        )
    }
}
export default BookingList;