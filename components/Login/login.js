import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text, AsyncStorage,
    StatusBar,
    Image, ActivityIndicator,
    TouchableOpacity,
    TextInput,
    ImageBackground,
    ToastAndroid,
} from 'react-native';
import styles from './loginStyle';
import mainStyles from '../mainStyle';
// import { createStackNavigator } from '@react-navigation/stack';
// import { NavigationContainer } from '@react-navigation/native';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phoneNumber: '',
            // Password: '',
            ShowPassword: true,
            error: false,
            spinner: false,
            logout: ''
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('logout', async (err, result) => {
            var res = await JSON.parse(result);
            this.setState({logout: res})
            if(res == 'no') {
                this.props.navigation.navigate('Bhome', {details: "hello"})
            }
            else {
                console.log("still login")
            }
        })
    }
    
    clickLogin = () => {
        console.log(this.state.phoneNumber, 'dai')
        this.setState({ error: true, spinner: true, })
        if (this.state.phoneNumber == "" || this.state.phoneNumber.length < 10) {
            this.setState({ error: true, spinner: false, })
        }
        else {
            fetch('https://keralawings.co.in/booking/api/login_otp_send', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'mobile=' + this.state.phoneNumber 
            }).then((response) => response.json())
                .then(async (responseJson) => {
                    console.log(responseJson, 'hello res ot');
                    if (responseJson.status == true) {
                        await AsyncStorage.setItem('userdetails', JSON.stringify(responseJson.userdetails));
                        this.props.navigation.navigate('OTP', {otp: responseJson.otp, name: responseJson.userdetails.name })
                        // ToastAndroid.showWithGravity(
                        //     'Login successfully',
                        //     ToastAndroid.SHORT,
                        //     ToastAndroid.CENTER,
                        // );
                        this.setState({
                            spinner: false
                        })
                    }
                    else {
                        ToastAndroid.showWithGravity(
                            'please enter valid user details',
                            ToastAndroid.SHORT,
                            ToastAndroid.CENTER,
                        );
                        this.setState({
                            spinner: false
                        })
                    }


                }).catch((error) => {
                    console.warn(error, 'error');
                });
        }
    }

    showPassword = () => {
        this.setState({
            ShowPassword: !this.state.ShowPassword
        });
    }

    clickRegister = () => {
        this.props.navigation.navigate('Register')
    }
    /* <View style={mainStyles.loader}>
        <Image
            source={require('../../assets/icons/loader.gif')}
            style={{ width: 50, height: 50 }} />
       
        <Text style={mainStyles.spinnerTextStyle}>please wait</Text>
    </View> */

    render() {
        console.log(this.state.ShowPassword, 'this.state.ShowPassword');
        return (
            <View style={{ backgroundColor: '#f5f5f5' }}>
                {this.state.spinner ?
                    <View style={{ marginTop: "80%", position: 'absolute', zIndex: 99, width: "100%", paddingHorizontal: 20, height: 70 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', }}>
                            <Image
                                source={require('../../assets/images/loader.gif')}
                                style={{ width: 200, height: 200 }} />
                            {/* <Text style={mainStyles.spinnerTextStyle}>please wait</Text> */}
                        </View>
                    </View>
                    :

                    <View style={{ height: '100%', alignItems: 'center', backgroundColor: '#ffffff', justifyContent: 'center' }}>
                        <View style={{ alignItems: 'center', marginTop: 10 }}>
                            <Image
                                source={require('../../assets/images/logo.jpg')}
                                style={{ width: 170, height: 150 }} />
                        </View>
                        <View style={{ paddingHorizontal: 20, width: '100%', }}>
                            <View style={styles.loginView}>
                                <View style={styles.emailSection}>
                                    <TextInput
                                        style={styles.input}
                                        placeholder="Enter phone number"
                                        placeholderTextColor="#919e9a"
                                        keyboardType='numeric'
                                        textT
                                        value={this.state.phoneNumber}
                                        onChangeText={(PhoneString) => { this.setState({ phoneNumber: PhoneString }) }}
                                    />
                                </View>
                                {this.state.error &&
                                    <View>
                                        {this.state.phoneNumber == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Enter the phone number</Text> :
                                            <Text style={{ display: 'none' }}></Text>
                                        }
                                    </View>
                                }

                                {this.state.error &&
                                    <View>
                                        {this.state.phoneNumber !== "" && this.state.phoneNumber.length < 10 ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Enter the 10 digit phone number</Text> :
                                            <Text style={{ display: 'none' }}></Text>
                                        }
                                    </View>
                                }
                                {/*                                 
                                <View style={styles.emailSection}>
                                    <TextInput
                                        style={styles.input}
                                        placeholder="Enter Password"
                                        placeholderTextColor="#1fa274"
                                        value={this.state.Password}
                                        onChangeText={(EmailString) => { this.setState({ Password: EmailString }) }}
                                    />
                                </View>
                                {this.state.error &&
                                    <View>
                                        {this.state.Password == "" ? <Text style={{ color: 'red', fontSize: 10, marginTop: 3 }}>Enter the Password</Text> :
                                            <Text style={{ display: 'none' }}></Text>
                                        }
                                    </View>
                                } */}

                            </View>
                            <TouchableOpacity onPress={this.clickLogin} style={styles.loginButton}>
                                {/* <Image
                                    source={require('../../assets/images/login2.png')}
                                    style={{ width: 90, height: 17 }} /> */}
                                <Text style={{ color: '#ffffff', fontSize: 14, letterSpacing: 0.5 }}>SUBMIT</Text>
                            </TouchableOpacity>

                            {/* <View style={{ display: 'flex', flexDirection: 'row', marginTop: 5, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 10 }}>If you are new user please</Text>
                                <TouchableOpacity onPress={this.clickRegister} style={{ marginLeft: 10 }}>
                                    <Text style={{ color: '#02bcb1', fontWeight: 'bold' }}>Register</Text>
                                </TouchableOpacity>
                            </View> */}

                        </View>

                    </View>
                }
            </View>
        )
    }
}
export default Login;



